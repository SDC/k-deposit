<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8" >
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <form class="m-2 border" method="POST" action="{{ route('deposits.store') }}">
                    @csrf
                    @method('POST')
                    <div class="m-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <div class="mb-3 ">
                                        <label class="form-label">Particulars</label>
                                        <input type="text" class="form-control form-control{{($errors->first('particulars') ? 'is-dangerous' : '')}}" name="particulars">
                                        <div class="form-text">Enter particulars</div>
                                        <p class="help" style="color:red">{{ $errors->first('particulars') }}</p>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="mb-3 ">
                                        <label class="form-label">K-depo/D-3</label>
                                        <select class="form-select form-control{{($errors->first('depo_d3') ? 'is-dangerous' : '')}}" name="depo_d3">
                                            <option selected disabled>Click to open select menu</option>
                                            <option value="K-Deposit">K-Deposit</option>
                                            <option value="D-3">D-3</option>
                                        </select>
                                        <div class="form-text">Choose One</div>
                                        <p class="help" style="color:red">{{ $errors->first('depo_d3') }}</p>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="mb-3 ">
                                        <label class="form-label">Department Name</label>
                                        <input name="department" type="text" class="form-control form-control{{($errors->first('department_name') ? 'is-dangerous' : '')}}" value="{{ $department->name }}">
                                        <div class="form-text">Department Name</div>
                                        <p class="help" style="color:red">{{ $errors->first('department_name') }}</p>
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm">
                                    <div class="mb-3 ">
                                        <label class="form-label">Challan Number</label>
                                        <input type="text" class="form-control form-control{{($errors->first('challan_number') ? 'is-dangerous' : '')}}" name="challan_number">
                                        <div class="form-text">Enter Challan Number</div>
                                        <p class="help" style="color:red">{{ $errors->first('challan_number') }}</p>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="mb-3 ">
                                        <label class="form-label">Creation Date</label>
                                        <input type="date" class="form-control form-control{{($errors->first('create_date') ? 'is-dangerous' : '')}}" name="create_date" id='date' data-date-format="DD/MM/YYYY">
                                        <div class="form-text">Enter Creation date</div>
                                        <p class="help" style="color:red">{{ $errors->first('create_date') }}</p>
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-sm">
                                    <div class="mb-3 ml-2 mr-2">
                                        <label class="form-label">Challan Amount</label>
                                        <input type="text" class="form-control form-control{{($errors->first('challan_amount') ? 'is-dangerous' : '')}}" name="challan_amount" id="CA" onkeyup="sum();">
                                        <div class="form-text">Enter Challan Amount</div>
                                        <p class="help" style="color:red">{{ $errors->first('challan_amount') }}</p>
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div class="mb-3 ml-2 mr-2">
                                        <label class="form-label">Withdrawn Amount</label>
                                        <input type="text" class="form-control form-control{{($errors->first('withdrawn_amount') ? 'is-dangerous' : '')}}" name="withdrawn_amount" value='0'></textarea>
                                        <div class="form-text">Enter Withdrawn Amount</div>
                                        <p class="help" style="color:red">{{ $errors->first('withdrawn_amount') }}</p>
                                    </div>
                                </div>

                                <div class="col-sm">
                                    <div class="mb-3 ml-2 mr-2">
                                        <label class="form-label">Balance</label>
                                        <input type="text" class="form-control form-control{{($errors->first('balance') ? 'is-dangerous' : '')}}" name="balance" id="balance"></textarea>
                                        <div class="form-text">Enter Balance</div>
                                        <p class="help" style="color:red">{{ $errors->first('balance') }}</p>
                                    </div>
                                </div>

                                
                              
                                <script>
                                    function sum() {
                                        var txtFirstNumberValue = document.getElementById('CA').value;
                                        
                                        var result = parseInt(txtFirstNumberValue);
                                        if (!isNaN(result)) {
                                            document.getElementById('balance').value = result;
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="mb-3 ml-2 mr-2">
                                <label class="form-label">Treasury</label>
                                <select class="form-select form-control{{($errors->first('treasury') ? 'is-dangerous' : '')}}" name="treasury" id="treasury">
                                    <option selected disabled>Select Treasury</option>
                                    <option value="Aizawl South Treasury">Aizawl South Treasury</option>
                                    <option value="Aizawl North Treasury">Aizawl North Treasury</option>
                                    <option value="Lunglei Treasury">Lunglei Treasury</option>
                                    <option value="Siaha Treasury">Siaha Treasury</option>
                                    <option value="Champhai Treasury">Champhai Treasury</option>
                                    <option value="Kolasib  Treasury">Kolasib Treasury</option>
                                    <option value="Serchhip Treasury">Serchhip Treasury</option>
                                    <option value="Lawngtlai Treasury">Lawngtlai Treasury</option>
                                    <option value="Mamit Treasury">Mamit Treasury</option>
                                    <option value="Chawngte Treasury">Chawngte Treasury</option>                                
                                </select>
                                    <div class="form-text">Enter treasury</div>
                                <p class="help" style="color:red">{{ $errors->first('treasury') }}</p>
                            </div>
                        </div>
                       
                        
                        

                        
                       
                        

                        <input type="hidden" value="{{ $department->id }}" class="ml-2 mr-2" name="department_id"><br>
                        <button type="submit" class="btn btn-primary ml-2 mr-2">Submit</button>
                    </div>

                    
                    
                </form>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $("id").on("change", function() {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format( this.getAttribute("data-date-format") )
        )
    }).trigger("change")
</script>

<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
      alert(msg);
    }
  </script>