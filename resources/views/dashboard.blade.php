<x-app-layout>
    {{-- <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot> --}}

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <form class="d-flex mb-5 container" method="GET" action="{{ route('challan.search') }}">
                            <input class="form-control me-2" type="search" placeholder="Search Challan Number" aria-label="Search" style="width: 250px" name="c_number" required>
                            <button class="btn btn-outline-primary" type="submit">Search</button>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <form class="d-flex mb-5 container" method="GET" action="{{ route('date.search') }}">
                            <input type="date" class="form-control" name="d_date" id='date' data-date-format="DD/MM/YYYY" data-date="" required>
                            <button class="btn btn-outline-primary" type="submit">Search</button>
                        </form>
                    </div>
                 
                    <div class="col-md-4">
                        <form class="d-flex mb-5 container" method="GET" action="{{ route('memo.search') }}">
                            <input type="text" class="form-control" name="memo" id='memo' required>
                            <button class="btn btn-outline-primary" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </div>
            {{-- <div style="padding-left:370px">
                
                <form class="d-flex mb-5 container" method="GET" action="{{ route('challan.search') }}">
                    <input class="form-control me-2" type="search" placeholder="Search Challan Number" aria-label="Search" style="width: 250px" name="c_number" required>
                    <button class="btn btn-outline-primary" type="submit">Search</button>
                </form>
            </div> --}}
            
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="container mt-3 mb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <form class="d-flex">
                                    <input class="form-control me-2" type="search" placeholder="Search Department" aria-label="Search" id="myInput" onkeyup="myFunction()">
                                </form>
                            </div>
                            <div class="col-sm">
                                <a class="btn btn-primary btn-sm mb-3 float-right" href="{{ route('departments.create') }}">New Department</a>
                            </div>
                           
                        </div>
                      </div>
                    
                    <table class="table table-hover table-bordered align-middle" id="myTable">
                        <thead>
                          <tr>
                            <th scope="col">SI</th>
                            <th scope="col">Department Name</th>
                            <th>Abbreviation</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @php
                                $i=1;
                            @endphp 
                            @foreach ($departments as $department)
                            <tr>
                                <th scope="row">
                                    @php
                                        echo $i++;
                                    @endphp 
                                </th>
                                <td>{{ $department->name }}</td>
                                <td>{{ $department->abbreviation }}</td>
                                <td class="flex flex-row">
                                    <a type="button" class="btn btn-outline-success btn-sm" href="{{ route('departments.edit',$department->id) }}"> 
                                        <i class="fas fa-pen-square mr-2 fa-lg float-center icon_at_center" ></i>
                                    </a>
                                   
                                    
                                    <form action="{{ route('departments.destroy',$department->id) }}" method="POST" class="ml-2 mr-2">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure?')">
                                            <i class="fas fa-calendar-times mr-2 fa-lg icon_at_center">
                                            </i>
                                        </button>
                                    </form>
                                    
                                    <a type="button" class="btn btn-outline-primary btn-sm" href="{{ route('deposits.mainindex',$department->id) }}"><i class="fas fa-location-arrow fa-lg icon_at_center" style="padding-right:7px"></i></a>
                                </td>
                               
                                   
                                    
                            </tr>
                            @endforeach
                            
                          
                          
                        </tbody>
                    </table>
                    
                </div>
                
            </div>
        </div>
    </div>
</x-app-layout>
<style>
    .icon_at_center{
        padding-left:7px;
        padding-top:7px;
    }
</style>

<script>
    function myFunction() {
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
    </script>


<script>
    $("id").on("change", function() {
        this.setAttribute(
            "data-date-format",
            moment(this.value, "YYYY-MM-DD")
            .format( this.getAttribute("data-date-format") )
        )
    }).trigger("change")
</script>