<x-app-layout>
    <div class="py-12">
        <div class="max-w-4xl mx-auto sm:px-6 lg:px-8" style="padding-top:100px">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <form method="POST" action="{{ route('departments.update',$department->id) }}" class="m-5">
                   
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="exampleInputEmail1">Department Name</label>
                        <input type="text" class="form-control"  value="{{ $department->name }}" name="name"required>
                        <small id="emailHelp" class="form-text text-muted">Edit department name and click 'Update Button'</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Abbreviation</label>
                        <input type="text" class="form-control"  value="{{ $department->abbreviation }}" required name="abbreviation">
                        <small id="emailHelp" class="form-text text-muted">Enter abbreviation and click 'Add Button'</small>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
