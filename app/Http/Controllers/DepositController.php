<?php

namespace App\Http\Controllers;
use App\Models\Department;
use App\Models\Deposit;
use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\DB;
class DepositController extends Controller
{
    public function mainindex($id)
    {
        $department=Department::findOrFail($id);
        $deposits=Deposit::where('department_id',$id)->paginate(7);
        return view('deposit.index',compact('department','deposits'));
    }

    public function maincreate($id)
    {
        $department=Department::findOrFail($id);
        return view('deposit.create',compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'particulars'=>'required', 
            'department'=>'required',
            'challan_number'=>'required',
            'create_date'=>'required',
            'challan_amount'=>'required',
            'withdrawn_amount'=>'required',
            'balance'=>'required',
            'department_id'=>'required',
            'treasury'=>'required',
            'depo_d3'=>'required',
        ]);

        $timestamp = strtotime($request->create_date);

        //Convert it to DD-MM-YYYY
        $dmy = date("d-m-Y", $timestamp); 

        $search=Deposit::where('treasury',$request->treasury)->get();
        foreach($search as $s)
        {
            if($s->challan_number==$request->challan_number && $s->create_date==$dmy)
            {
                return redirect()->back() ->with('alert', 'Cannot have the same date, challan no. and Treasury!');
            }
            
        }
       
        // if() 
        //     return redirect()->back() ->with('alert', 'Cannot have the same date, challan no. and Treasury!');
        // else
        //     dd("Nothing");
        


        // dd($request);
        Deposit::create([
            'particulars'=>$request->particulars, 
            'depo_d3'=>$request->depo_d3,
            'department'=>$request->department,
            
            'challan_number'=>$request->challan_number,
            'create_date'=>$dmy,
            'challan_amount'=>$request->challan_amount,
            'balance'=>$request->balance,
            'withdrawn_amount'=>$request->withdrawn_amount,
            'department_id'=>$request->department_id,
            'treasury'=>$request->treasury,
            
        ]);
            
        $department=Department::where('id',$request->department_id)->first();
        // dd($department);
        // $deposit=Deposit::where('department_id',$id)->latest()->first();
        // dd($deposit->id);
        $deposits=Deposit::where('department_id',$request->department_id)->paginate(7);
        return view('deposit.index',compact('department','deposits'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deposit=Deposit::findOrFail($id);
        
        return view('deposit.edit', compact('deposit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'particulars'=>'required', 
            'department'=>'required',
            'challan_number'=>'required',
            'create_date'=>'required',
            'challan_amount'=>'required',
            'treasury'=>'required',
            'balance'=>'required',
            'withdrawn_amount'=>'required',
            'department_id'=>'required',
            
        ]);

        $timestamp = strtotime($request->create_date);

        //Convert it to DD-MM-YYYY
        $dmy = date("d-m-Y", $timestamp);

        $update = [
            'particulars'=>$request->particulars, 
            'department'=>$request->department,
            'challan_number'=>$request->challan_number,
            'create_date'=>$dmy,
            'challan_amount'=>$request->challan_amount,
            'balance'=>$request->balance,
            'withdrawn_amount'=>$request->withdrawn_amount,
            'department_id'=>$request->department_id,
            'treasury'=>$request->treasury,
        ];
        Deposit::where('id',$id)->update($update);

        
        // dd($request->department_id);
        $department=Department::where('id',$request->department_id)->first();
        // dd($department->department_name);
        $deposits=Deposit::where('department_id',$request->department_id)->paginate(7);
        return view('deposit.index',compact('department','deposits'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $depoDept=Deposit::where('id',$id)->pluck('department_id');
        $departments=Department::all();
        Deposit::where('id',$id)->delete();
        
        

       
        return redirect()->route('dashboard',compact('departments'));
    }

    public function search(Request $request)
    {
        $deposits=Deposit::where('challan_number',$request->c_number)->firstOrFail();
        // dd($deposits->id);
        $department=Department::where('id',$deposits->department_id)->first();
        // dd($department->name);
        return view('deposit.search',compact('department','deposits'));
        // dd($deposits->department_id);
    }


    public function date_search(Request $request)
    {
        $timestamp = strtotime($request->d_date);
        $dmy = date("d-m-Y", $timestamp);
        // dd($dmy);
        $deposits=Deposit::where('create_date',$dmy)->get();
        
        // dd($deposits);
        return view('deposit.search_by_date',compact('deposits'));
    }



    
}
