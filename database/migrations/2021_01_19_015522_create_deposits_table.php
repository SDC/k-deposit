<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->id();
            $table->string('particulars',150);
            $table->string('depo_d3',50);
            $table->string('department',50);
            $table->string('challan_number',50);
            $table->string('create_date',50);
            $table->double('challan_amount');
            $table->double('withdrawn_amount');
            $table->double('balance');
            $table->string('treasury');
            $table->unsignedBigInteger('department_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
